#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include "gatt_api_server.h"

ssize_t send_callback(nghttp2_session *session, const uint8_t *data,
                      size_t length, int flags, void *user_data)
{
    http2_session_data *session_data = (http2_session_data *)user_data;
    struct bufferevent *bev = session_data->bev;
    (void)session;
    (void)flags;

    /* Avoid excessive buffering in server side. */
    if (evbuffer_get_length(bufferevent_get_output(session_data->bev)) >=
        OUTPUT_WOULDBLOCK_THRESHOLD)
    {
        return NGHTTP2_ERR_WOULDBLOCK;
    }
    bufferevent_write(bev, data, length);
    return (ssize_t)length;
}

ssize_t file_read_callback(nghttp2_session *session, int32_t stream_id,
                           uint8_t *buf, size_t length,
                           uint32_t *data_flags,
                           nghttp2_data_source *source,
                           void *user_data)
{
    int fd = source->fd;
    ssize_t r;
    (void)session;
    (void)stream_id;
    (void)user_data;

    while ((r = read(fd, buf, length)) == -1 && errno == EINTR)
        ;
    if (r == -1)
    {
        return NGHTTP2_ERR_TEMPORAL_CALLBACK_FAILURE;
    }
    if (r == 0)
    {
        *data_flags |= NGHTTP2_DATA_FLAG_EOF;
    }
    return r;
}

/* nghttp2_on_data_chunk_recv_callback: Called when DATA frame is
   received from the remote peer. In this implementation, if the frame
   is meant to the stream we initiated, print the received data in
   stdout, so that the user can redirect its output to the file
   easily. */
int on_data_chunk_recv_callback(nghttp2_session *session, uint8_t flags,
                                int32_t stream_id, const uint8_t *data,
                                size_t len, void *user_data)
{
    http2_session_data *session_data = (http2_session_data *)user_data;
    http2_stream_data *stream_data;
    (void)session;
    (void)flags;

    char *userdata = malloc(len + 1);
    memset(userdata, 0, len + 1);
    memcpy(userdata, data, len);
#ifdef DEBUG
    printf("[DEBUG] Received data : [%s] (%lu bytes)\n", userdata, len);
#endif /** DEBUG */
    stream_data =
        nghttp2_session_get_stream_user_data(session, stream_id);
    if (!stream_data)
    {
        return 0;
    }
    stream_data->body = userdata;
    return on_request_recv(session, session_data, stream_data);
}

int on_frame_recv_callback(nghttp2_session *session,
                           const nghttp2_frame *frame, void *user_data)
{
    http2_session_data *session_data = (http2_session_data *)user_data;
    http2_stream_data *stream_data;
    switch (frame->hd.type)
    {
    case NGHTTP2_DATA:
        break;
    case NGHTTP2_HEADERS:
        /* Check that the client request has finished */
        if (frame->hd.flags & NGHTTP2_FLAG_END_STREAM)
        {
            stream_data =
                nghttp2_session_get_stream_user_data(session, frame->hd.stream_id);
            /* For DATA and HEADERS frame, this callback may be called after
         on_stream_close_callback. Check that stream still alive. */
            if (!stream_data)
            {
                return 0;
            }
            return on_request_recv(session, session_data, stream_data);
        }
        break;
    default:
        break;
    }
    return 0;
}

int on_stream_close_callback(nghttp2_session *session, int32_t stream_id,
                             uint32_t error_code, void *user_data)
{
    http2_session_data *session_data = (http2_session_data *)user_data;
    http2_stream_data *stream_data;
    (void)error_code;

    stream_data = nghttp2_session_get_stream_user_data(session, stream_id);
    if (!stream_data)
    {
        return 0;
    }
    remove_stream(session_data, stream_data);
    delete_http2_stream_data(stream_data);
    return 0;
}

/* readcb for bufferevent after client connection header was
   checked. */
void readcb(struct bufferevent *bev, void *ptr)
{
    http2_session_data *session_data = (http2_session_data *)ptr;
    (void)bev;

    if (session_recv(session_data) != 0)
    {
        delete_http2_session_data(session_data);
        return;
    }
}

/** 
 * writecb for bufferevent. To greaceful shutdown after sending or
 * receiving GOAWAY, we check the some conditions on the nghttp2
 * library and output buffer of bufferevent. If it indicates we have
 * no business to this session, tear down the connection. If the
 * connection is not going to shutdown, we call session_send() to
 * process pending data in the output buffer. This is necessary
 * because we have a threshold on the buffer size to avoid too much
 * buffering. See send_callback(). 
 */
void writecb(struct bufferevent *bev, void *ptr)
{
    http2_session_data *session_data = (http2_session_data *)ptr;
    if (evbuffer_get_length(bufferevent_get_output(bev)) > 0)
    {
        return;
    }
    if (nghttp2_session_want_read(session_data->session) == 0 &&
        nghttp2_session_want_write(session_data->session) == 0)
    {
        delete_http2_session_data(session_data);
        return;
    }
    if (session_send(session_data) != 0)
    {
        delete_http2_session_data(session_data);
        return;
    }
}

/* eventcb for bufferevent */
void eventcb(struct bufferevent *bev, short events, void *ptr)
{
    http2_session_data *session_data = (http2_session_data *)ptr;
    if (events & BEV_EVENT_CONNECTED)
    {
        const unsigned char *alpn = NULL;
        unsigned int alpnlen = 0;
        SSL *ssl;
        (void)bev;

#if DEBUG
        fprintf(stderr, "[INFO] %s connected\n", session_data->client_addr);
#endif /** DEBUG */
        ssl = bufferevent_openssl_get_ssl(session_data->bev);

#ifndef OPENSSL_NO_NEXTPROTONEG
        SSL_get0_next_proto_negotiated(ssl, &alpn, &alpnlen);
#endif /* !OPENSSL_NO_NEXTPROTONEG */
#if OPENSSL_VERSION_NUMBER >= 0x10002000L
        if (alpn == NULL)
        {
            SSL_get0_alpn_selected(ssl, &alpn, &alpnlen);
        }
#endif /* OPENSSL_VERSION_NUMBER >= 0x10002000L */

        if (alpn == NULL || alpnlen != 2 || memcmp("h2", alpn, 2) != 0)
        {
            fprintf(stderr, "[ERROR] %s h2 is not negotiated\n", session_data->client_addr);
            delete_http2_session_data(session_data);
            return;
        }

        initialize_nghttp2_session(session_data);

        if (send_server_connection_header(session_data) != 0 ||
            session_send(session_data) != 0)
        {
            delete_http2_session_data(session_data);
            return;
        }

        return;
    }
    if (events & BEV_EVENT_EOF)
    {
#if DEBUG
        printf("[DEBUG] end connection for %s\n", session_data->client_addr);
#endif /** DEBUG */
    }
    else if (events & BEV_EVENT_ERROR)
    {
        fprintf(stderr, "[ERROR] %s network error\n", session_data->client_addr);
    }
    else if (events & BEV_EVENT_TIMEOUT)
    {
        fprintf(stderr, "[ERROR] client %s timeout\n", session_data->client_addr);
    }
    delete_http2_session_data(session_data);
}

/* acceptcb is the callback for evconnlistener */
void acceptcb(struct evconnlistener *listener, int fd,
              struct sockaddr *addr, int addrlen, void *arg)
{
    app_context *app_ctx = (app_context *)arg;
    http2_session_data *session_data;
    (void)listener;

    session_data = create_http2_session_data(app_ctx, fd, addr, addrlen);

    bufferevent_setcb(session_data->bev, readcb, writecb, eventcb, session_data);
}

/** 
 * on_header_callback is called when nghttp2 library emits
 * single header name/value pair. Used to load all header attributes info
 * in dedicated stream_data struct fields
*/
int on_header_callback(nghttp2_session *session,
                       const nghttp2_frame *frame, const uint8_t *name,
                       size_t namelen, const uint8_t *value,
                       size_t valuelen, uint8_t flags, void *user_data)
{
    http2_stream_data *stream_data;
    const char PATH[] = ":path";
    const char METHOD[] = ":method";
    const char USERAGENT[] = "user-agent";
    const char AUTHORIZATION[] = "authorization";
    (void)flags;
    (void)user_data;

    switch (frame->hd.type)
    {
    case NGHTTP2_HEADERS:
#ifdef DEBUG
        printf("[DEBUG] got header [%s]:[%s]\n", name, value);
#endif /** DEBUG */
        stream_data =
            nghttp2_session_get_stream_user_data(session, frame->hd.stream_id);
        if (!stream_data)
        {
            break;
        }
        if (namelen == sizeof(PATH) - 1 && memcmp(PATH, name, namelen) == 0)
        {
            size_t j;
            for (j = 0; j < valuelen && value[j] != '?'; ++j)
                ;
            stream_data->request_path = percent_decode(value, j);
        }
        if (namelen == sizeof(METHOD) - 1 && memcmp(METHOD, name, namelen) == 0)
        {
            char *methodName = malloc(valuelen + 1);
            memset(methodName, 0, valuelen + 1);
            memcpy(methodName, value, valuelen);
            stream_data->method = methodName;
        }
        if (namelen == sizeof(USERAGENT) - 1 && memcmp(USERAGENT, name, namelen) == 0)
        {
            char *userAgent = malloc(valuelen + 1);
            memset(userAgent, 0, valuelen + 1);
            memcpy(userAgent, value, valuelen);
#ifdef DEBUG
            printf("[DEBUG] Will save session user agent header [%s]\n", userAgent);
#endif /** DEBUG */

            stream_data->user_agent = userAgent;
        }
        if (namelen == sizeof(AUTHORIZATION) - 1 && memcmp(AUTHORIZATION, name, namelen) == 0)
        {
            char *authorization = malloc(valuelen + 1);
            memset(authorization, 0, valuelen + 1);
            memcpy(authorization, value, valuelen);
#ifdef DEBUG
            printf("[DEBUG] Will save session authorization header [%s]\n", authorization);
#endif /** DEBUG */
            stream_data->authorization = authorization;
        }
        break;
    }
    return 0;
}

int on_begin_headers_callback(nghttp2_session *session,
                              const nghttp2_frame *frame,
                              void *user_data)
{
    http2_session_data *session_data = (http2_session_data *)user_data;
    http2_stream_data *stream_data;

    if (frame->hd.type != NGHTTP2_HEADERS ||
        frame->headers.cat != NGHTTP2_HCAT_REQUEST)
    {
        return 0;
    }
    stream_data = create_http2_stream_data(session_data, frame->hd.stream_id);
    nghttp2_session_set_stream_user_data(session, frame->hd.stream_id,
                                         stream_data);
    return 0;
}

/* Send HTTP/2 client connection header, which includes 24 bytes
   magic octets and SETTINGS frame */
int send_server_connection_header(http2_session_data *session_data)
{
    nghttp2_settings_entry iv[1] = {
        {NGHTTP2_SETTINGS_MAX_CONCURRENT_STREAMS, 100}};
    int rv;

    rv = nghttp2_submit_settings(session_data->session, NGHTTP2_FLAG_NONE, iv,
                                 ARRLEN(iv));
    if (rv != 0)
    {
        warnx("[ERROR] Fatal error: %s", nghttp2_strerror(rv));
        return -1;
    }
    return 0;
}