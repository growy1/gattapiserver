#include <err.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include "gatt_api_server.h"

http2_session_data *create_http2_session_data(app_context *app_ctx,
                                              int fd,
                                              struct sockaddr *addr,
                                              int addrlen)
{
    int rv;
    SSL *ssl;
    int val = 1;
    char host[NI_MAXHOST];
    http2_session_data *session_data;

    ssl = create_ssl(app_ctx->ssl_ctx);
    session_data = malloc(sizeof(http2_session_data));
    memset(session_data, 0, sizeof(http2_session_data));
    session_data->app_ctx = app_ctx;
    setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *)&val, sizeof(val));
    session_data->bev = bufferevent_openssl_socket_new(
        app_ctx->evbase, fd, ssl, BUFFEREVENT_SSL_ACCEPTING,
        BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    bufferevent_enable(session_data->bev, EV_READ | EV_WRITE);
    rv = getnameinfo(addr, (socklen_t)addrlen, host, sizeof(host), NULL, 0,
                     NI_NUMERICHOST);
    if (rv != 0)
    {
        session_data->client_addr = strdup("(unknown)");
    }
    else
    {
        session_data->client_addr = strdup(host);
    }

    return session_data;
}

void delete_http2_session_data(http2_session_data *session_data)
{
    http2_stream_data *stream_data;
    SSL *ssl = bufferevent_openssl_get_ssl(session_data->bev);
#if DEBUG
    fprintf(stderr, "[DEBUG] client %s disconnected\n", session_data->client_addr);
#endif /** DEBUG */
    if (ssl)
    {
        SSL_shutdown(ssl);
    }
    bufferevent_free(session_data->bev);
    nghttp2_session_del(session_data->session);
    for (stream_data = session_data->root.next; stream_data;)
    {
        http2_stream_data *next = stream_data->next;
        delete_http2_stream_data(stream_data);
        stream_data = next;
    }
    free(session_data->client_addr);
    free(session_data);
}

/* Serialize the frame and send (or buffer) the data to
   bufferevent. */
int session_send(http2_session_data *session_data)
{
    int rv;
    rv = nghttp2_session_send(session_data->session);
    if (rv != 0)
    {
        warnx("[ERROR] Fatal error: %s", nghttp2_strerror(rv));
        return -1;
    }
    return 0;
}

/* Read the data in the bufferevent and feed them into nghttp2 library
   function. Invocation of nghttp2_session_mem_recv() may make
   additional pending frames, so call session_send() at the end of the
   function. */
int session_recv(http2_session_data *session_data)
{
    ssize_t readlen;
    struct evbuffer *input = bufferevent_get_input(session_data->bev);
    size_t datalen = evbuffer_get_length(input);
    unsigned char *data = evbuffer_pullup(input, -1);

    readlen = nghttp2_session_mem_recv(session_data->session, data, datalen);
    if (readlen < 0)
    {
        warnx("[ERROR] Fatal error: %s", nghttp2_strerror((int)readlen));
        return -1;
    }
    if (evbuffer_drain(input, (size_t)readlen) != 0)
    {
        warnx("[ERROR] Fatal error: evbuffer_drain failed");
        return -1;
    }
    if (session_send(session_data) != 0)
    {
        return -1;
    }
    return 0;
}