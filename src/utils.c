#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "gatt_api_server.h"

/**
 * Remove substring sub from string str 
 */
char *strremove(char *str, const char *sub)
{
    char *p, *q, *r;
    if ((q = r = strstr(str, sub)) != NULL)
    {
        size_t len = strlen(sub);
        while ((r = strstr(p = r + len, sub)) != NULL)
        {
            while (p < r)
                *q++ = *p++;
        }
        while ((*q++ = *p++) != '\0')
            continue;
    }
    return str;
}

/* Minimum check for directory traversal. Returns nonzero if it is
   safe. */
int check_path(const char *path)
{
    /* We don't like '\' in url. */
    return path[0] && path[0] == '/' && strchr(path, '\\') == NULL &&
           strstr(path, "/../") == NULL && strstr(path, "/./") == NULL &&
           !ends_with(path, "/..") && !ends_with(path, "/.");
}

/* Returns nonzero if the string |s| ends with the substring |sub| */
int ends_with(const char *s, const char *sub)
{
    size_t slen = strlen(s);
    size_t sublen = strlen(sub);
    if (slen < sublen)
    {
        return 0;
    }
    return memcmp(s + slen - sublen, sub, sublen) == 0;
}

/* Returns int value of hex string character |c| */
uint8_t hex_to_uint(uint8_t c)
{
    if ('0' <= c && c <= '9')
    {
        return (uint8_t)(c - '0');
    }
    if ('A' <= c && c <= 'F')
    {
        return (uint8_t)(c - 'A' + 10);
    }
    if ('a' <= c && c <= 'f')
    {
        return (uint8_t)(c - 'a' + 10);
    }
    return 0;
}

/* Decodes percent-encoded byte string |value| with length |valuelen|
   and returns the decoded byte string in allocated buffer. The return
   value is NULL terminated. The caller must free the returned
   string. */
char *percent_decode(const uint8_t *value, size_t valuelen)
{
    char *res;

    res = malloc(valuelen + 1);
    if (valuelen > 3)
    {
        size_t i, j;
        for (i = 0, j = 0; i < valuelen - 2;)
        {
            if (value[i] != '%' || !isxdigit(value[i + 1]) ||
                !isxdigit(value[i + 2]))
            {
                res[j++] = (char)value[i++];
                continue;
            }
            res[j++] =
                (char)((hex_to_uint(value[i + 1]) << 4) + hex_to_uint(value[i + 2]));
            i += 3;
        }
        memcpy(&res[j], &value[i], 2);
        res[j + 2] = '\0';
    }
    else
    {
        memcpy(res, value, valuelen);
        res[valuelen] = '\0';
    }
    return res;
}

/**
 * string2hexString convert an ascii string to it's hexadecimal equivalent 
*/
void string2hexString(char *input, char *output)
{
    int loop;
    int i;

    i = 0;
    loop = strlen(input) - 1;
#if DEBUG
    printf("[DEBUG] Will convert %i bytes\n", loop);
#endif
    while (input[loop] != '\0')
    {
        sprintf((char *)(output + i), "%02X", input[loop]);
        loop -= 1;
        i += 2;
        if (loop <= 0)
            break;
    }
    //insert NULL at the end of the output string
    output[i + 1] = '\0';
#if DEBUG
    printf("[DEBUG] Got ouput [%s]\n", output);
#endif
}