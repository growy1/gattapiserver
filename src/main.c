#ifdef __sgi
#define errx(exitcode, format, args...) \
	{                                   \
		warnx(format, ##args);          \
		exit(exitcode);                 \
	}
#define warn(format, args...) warnx(format ": %s", ##args, strerror(errno))
#define warnx(format, args...) fprintf(stderr, format "\n", ##args)
#endif

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif /* HAVE_NETDB_H */
#include <signal.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <sys/stat.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif /* HAVE_FCNTL_H */
#include <ctype.h>
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <netinet/tcp.h>
#ifndef __sgi
#include <err.h>
#endif

#include <string.h>
#include <errno.h>

#include <fcntl.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/mman.h>

#include "global.h"
#include "gatt_api_server.h"

/** SSL glob vars */
unsigned char next_proto_list[256];
size_t next_proto_list_len;
SSL_CTX *ssl_ctx;

/** Libevent glob var */
struct event_base *evbase;

/** GATT API glob vars */
char *api_user;
char *api_password;
char *deviceAddress;
gatt_connection_t *connection;

int send_response(nghttp2_session *session, int32_t stream_id,
				  nghttp2_nv *nva, size_t nvlen, int fd)
{
	int rv;
	nghttp2_data_provider data_prd;
	data_prd.source.fd = fd;
	data_prd.read_callback = file_read_callback;

	rv = nghttp2_submit_response(session, stream_id, nva, nvlen, &data_prd);
	if (rv != 0)
	{
		warnx("[ERROR] Fatal error: %s", nghttp2_strerror(rv));
		return -1;
	}
	return 0;
}

int unauthorized_error_reply(nghttp2_session *session,
							 http2_session_data *session_data,
							 http2_stream_data *stream_data)
{
	int rv;
	ssize_t writelen;
	int pipefd[2];
	nghttp2_nv hdrs[] = {MAKE_NV(":status", "401"),
						 MAKE_NV("server", "GattAPI"),
						 MAKE_NV("content-type", "application/json")};
	char bfr[MAX_ERROR_RESPONSE_LEN] = {0};
	sprintf(bfr, "{\"error\": \"%s\"}", "missing or invalid authorization");
	rv = pipe(pipefd);
	if (rv != 0)
	{
		warn("[ERROR] Could not create pipe");
		rv = nghttp2_submit_rst_stream(session, NGHTTP2_FLAG_NONE,
									   stream_data->stream_id,
									   NGHTTP2_INTERNAL_ERROR);
		if (rv != 0)
		{
			warnx("[ERROR] Fatal error: %s", nghttp2_strerror(rv));
			return EXIT_FAILURE;
		}
		return EXIT_SUCCESS;
	}
	writelen = write(pipefd[1], bfr, strlen(bfr));
	close(pipefd[1]);
	if (writelen != strlen(bfr))
	{
		close(pipefd[0]);
		return EXIT_FAILURE;
	}
	stream_data->fd = pipefd[0];
	fprintf(stdout, "[GATTAPI] %s | %s | 401 | \"%s\"\n", session_data->client_addr, stream_data->method, stream_data->request_path);
	if (send_response(session, stream_data->stream_id, hdrs, ARRLEN(hdrs),
					  pipefd[0]) != 0)
	{
		close(pipefd[0]);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int error_reply(nghttp2_session *session,
				http2_session_data *session_data,
				http2_stream_data *stream_data,
				const char *status,
				const char *message)
{
	int rv;
	ssize_t writelen;
	int pipefd[2];
	nghttp2_nv hdrs[] = {MAKE_NV(":status", "400"),
						 MAKE_NV("server", "GattAPI"),
						 MAKE_NV("content-type", "application/json")};
	char bfr[MAX_ERROR_RESPONSE_LEN] = {0};
	if ((strlen(message) + 16) > MAX_ERROR_RESPONSE_LEN)
	{
		fprintf(stderr, "[ERROR] Error message is too long (%lu)\n", strlen(message));
		return EXIT_FAILURE;
	}
	sprintf(bfr, "{\"error\": \"%s\"}", message);
	rv = pipe(pipefd);
	if (rv != 0)
	{
		warn("[ERROR] Could not create pipe");
		rv = nghttp2_submit_rst_stream(session, NGHTTP2_FLAG_NONE,
									   stream_data->stream_id,
									   NGHTTP2_INTERNAL_ERROR);
		if (rv != 0)
		{
			warnx("[ERROR] Fatal error: %s", nghttp2_strerror(rv));
			return EXIT_FAILURE;
		}
		return 0;
	}
	writelen = write(pipefd[1], bfr, strlen(bfr));
	close(pipefd[1]);
	if (writelen != strlen(bfr))
	{
		close(pipefd[0]);
		return EXIT_FAILURE;
	}
	stream_data->fd = pipefd[0];
	fprintf(stdout, "[GATTAPI] %s | %s | 400 | \"%s\"\n", session_data->client_addr, stream_data->method, stream_data->request_path);
	if (send_response(session, stream_data->stream_id, hdrs, ARRLEN(hdrs),
					  pipefd[0]) != 0)
	{
		close(pipefd[0]);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int on_request_recv(nghttp2_session *session,
					http2_session_data *session_data,
					http2_stream_data *stream_data)
{
	int fd;
	int rv;
	char *rel_path;
	nghttp2_nv hdrs[] = {
		MAKE_NV(":status", "200"),
		MAKE_NV("server", "GattAPI"),
		MAKE_NV("content-type", "application/json")};

	if (!stream_data->request_path)
	{
		if (error_reply(session, session_data, stream_data, "500", "bad request") != 0)
		{
			return NGHTTP2_ERR_CALLBACK_FAILURE;
		}
		return EXIT_SUCCESS;
	}
	/** Check authentification */
	if (!check_auth(stream_data->authorization))
	{
		if (unauthorized_error_reply(session, session_data, stream_data) != 0)
		{
			return NGHTTP2_ERR_CALLBACK_FAILURE;
		}
		return EXIT_SUCCESS;
	}
	/** Check path for path traversal exploit */
	if (!check_path(stream_data->request_path))
	{
		if (error_reply(session, session_data, stream_data, "500", "bad request") != 0)
		{
			return NGHTTP2_ERR_CALLBACK_FAILURE;
		}
		return EXIT_SUCCESS;
	}
	/** Remove first '/' from request path */
	for (rel_path = stream_data->request_path; *rel_path == '/'; ++rel_path)
		;
	bool validuid = false;
	uuid_t characteristicUUID;
	char *token, *str, *tofree;
	tofree = str = strdup(rel_path); // We own str's memory now.
	while ((token = strsep(&str, "/")))
	{
#ifdef DEBUG
		printf("[DEBUG] Url token [%s] len : %lu\n", token, strlen(token));
#endif /* DEBUG */
		if (strlen(token) == UUID_LENGHT)
		{
#ifdef DEBUG
			printf("[DEBUG] Found channel UUID\n");
#endif /* DEBUG */
			if (gattlib_string_to_uuid(token, strlen(token) + 1, &characteristicUUID) != GATTLIB_SUCCESS)
			{
				if (error_reply(session, session_data, stream_data, "400", "invalid uuid") != 0)
				{
					return NGHTTP2_ERR_CALLBACK_FAILURE;
				}
				return EXIT_SUCCESS;
			}
			else
			{
#ifdef DEBUG
				printf("[DEBUG] Uuid [%s] is valid\n", token);
#endif /* DEBUG */
				validuid = true;
			}
		}
	}
	free(tofree);
	char *json_resp;
	if (validuid && strcmp(stream_data->method, METHOD_GET) == 0)
	{
		unsigned char *gatt_message = read_message(characteristicUUID);
		if (gatt_message == NULL)
		{
			if (error_reply(session, session_data, stream_data, "400", "no message found, device seem down") != 0)
			{
				return NGHTTP2_ERR_CALLBACK_FAILURE;
			}
			return EXIT_SUCCESS;
		}
		json_resp = parse_and_json_format_temp_and_humidity(gatt_message);
		free(gatt_message);
	}
	else if (validuid && strcmp(stream_data->method, METHOD_POST) == 0 && stream_data->body)
	{
		char *tohexa = malloc(strlen(stream_data->body) * 2 + 4);
		memset(tohexa, '\0', strlen(stream_data->body) * 2 + 4);
		string2hexString(stream_data->body, tohexa);
#ifdef DEBUG
		printf("[DEBUG] Post data: Ascii [%s] Hexa [%s] (%lu bytes)\n", stream_data->body, tohexa, strlen(stream_data->body));
#endif /** DEBUG */
		int rt = write_message(characteristicUUID, tohexa);
		free(tohexa);
		if (rt != 0)
		{
			if (error_reply(session, session_data, stream_data, "500", "failed to write message") != 0)
			{
				return NGHTTP2_ERR_CALLBACK_FAILURE;
			}
			return EXIT_SUCCESS;
		}
		json_resp = strdup("{\"data\":\"successfully sent\"}");
	}
	else
	{
		int i, count = 0;
		char **characteristics = get_characteristics(&count);
		int buff_size = (UUID_LENGHT * count+1) + 5 + count;
		json_resp = malloc(buff_size);
		memset(json_resp, 0, buff_size);
		strcpy(json_resp, "[");
#ifdef DEBUG
		printf("[DEBUG] Found %i characteristics\n", count);
#endif /* DEBUG */
		for (; count >= 0; count--)
		{
			char tmp[UUID_LENGHT + 4] = {0};
			if (characteristics[count] != NULL)
			{
				sprintf(tmp, "\"%s\"", characteristics[count]);
				if (count - 1 >= 0)
				{
					strcat(tmp, ",");
				}
				strcat(json_resp, tmp);
			}
			free(characteristics[count]);
		}
		strcat(json_resp, "]");
		free(characteristics);
	}
	fd = memfd_create("tmpresp", FD_CLOEXEC);
	if (fd == -1)
	{
		fprintf(stderr, "[ERROR] Failed to create tmp fd : %s\n", strerror(errno));
		if (error_reply(session, session_data, stream_data, "500", "server error") != 0)
		{
			return NGHTTP2_ERR_CALLBACK_FAILURE;
		}
		return EXIT_SUCCESS;
	}
	int writed = write(fd, json_resp, strlen(json_resp));
	if (writed == EXIT_FAILURE)
	{
		fprintf(stderr, "[ERROR] Failed to write to tmp fd : %s\n", strerror(errno));
		if (error_reply(session, session_data, stream_data, "500", "server error") != 0)
		{
			return NGHTTP2_ERR_CALLBACK_FAILURE;
		}
		return EXIT_SUCCESS;
	}
	free(json_resp);
	lseek(fd, 0, SEEK_SET);
	stream_data->fd = fd;
	rv = send_response(session, stream_data->stream_id, hdrs, ARRLEN(hdrs), fd);
	if (rv != 0)
	{
		close(fd);
		fprintf(stderr, "[ERROR] Failed to send response : %s\n", nghttp2_strerror(rv));
		return NGHTTP2_ERR_CALLBACK_FAILURE;
	}
	char *tmpst = getrfc3339timestamp();
	fprintf(stdout, "[GATTAPI] %s | %s | %s | %s | 200 | \"%s\"\n", session_data->client_addr, tmpst, stream_data->user_agent, stream_data->method, stream_data->request_path);
	free(tmpst);
	return EXIT_SUCCESS;
}

void initialize_nghttp2_session(http2_session_data *session_data)
{
	nghttp2_session_callbacks *callbacks;

	nghttp2_session_callbacks_new(&callbacks);

	nghttp2_session_callbacks_set_send_callback(callbacks, send_callback);

	nghttp2_session_callbacks_set_on_data_chunk_recv_callback(
		callbacks, on_data_chunk_recv_callback);

	nghttp2_session_callbacks_set_on_frame_recv_callback(callbacks,
														 on_frame_recv_callback);

	nghttp2_session_callbacks_set_on_stream_close_callback(
		callbacks, on_stream_close_callback);

	nghttp2_session_callbacks_set_on_header_callback(callbacks,
													 on_header_callback);

	nghttp2_session_callbacks_set_on_begin_headers_callback(
		callbacks, on_begin_headers_callback);

	nghttp2_session_server_new(&session_data->session, callbacks, session_data);

	nghttp2_session_callbacks_del(callbacks);
}

static void start_listen(struct event_base *evbase, const char *service,
						 app_context *app_ctx)
{
	int rv;
	struct addrinfo hints;
	struct addrinfo *res, *rp;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
#ifdef AI_ADDRCONFIG
	hints.ai_flags |= AI_ADDRCONFIG;
#endif /* AI_ADDRCONFIG */

	rv = getaddrinfo(NULL, service, &hints, &res);
	if (rv != 0)
	{
		errx(1, "[ERROR] Could not resolve server address");
	}
	for (rp = res; rp; rp = rp->ai_next)
	{
		struct evconnlistener *listener;
		listener = evconnlistener_new_bind(
			evbase,
			acceptcb,
			app_ctx,
			LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE,
			16,
			rp->ai_addr,
			(int)rp->ai_addrlen);
		if (listener)
		{
			freeaddrinfo(res);
			fprintf(stdout, "[INFO] server listening on :%s\n", service);
			return;
		}
	}
	errx(1, "[ERROR] Could not start listener");
}

void initialize_app_context(app_context *app_ctx, SSL_CTX *ssl_ctx,
							struct event_base *evbase)
{
	memset(app_ctx, 0, sizeof(app_context));
	app_ctx->ssl_ctx = ssl_ctx;
	app_ctx->evbase = evbase;
}

void signal_handler(int sig, siginfo_t *siginfo, void *context)
{
	fprintf(stdout, "\r[INFO] Got SIGINT (%i), exiting ...", sig);
	gattlib_disconnect(connection);
	event_base_loopbreak(evbase);
	event_base_free(evbase);
	SSL_CTX_free(ssl_ctx);
	exit(EXIT_SUCCESS);
}

void *run(void *arg)
{
	app_context app_ctx;
	struct sigaction act;
	struct gatt_server_params *params = arg;

	memset(&act, 0, sizeof(struct sigaction));
	act.sa_sigaction = &signal_handler;
	act.sa_flags = SA_SIGINFO;

	ssl_ctx = create_ssl_ctx(params->key_file, params->cert_file);
	evbase = event_base_new();
	initialize_app_context(&app_ctx, ssl_ctx, evbase);
	start_listen(evbase, params->listening_port, &app_ctx);
	if (sigaction(SIGINT, &act, NULL) == -1)
	{
		fprintf(stderr, "[ERROR] sigaction failed : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	event_base_loop(evbase, 0);
	return NULL;
}

/**
 * usage print server cmdline usage
*/
static void usage(char *progName)
{
	printf("%s KEY_FILE CERT_FILE DEVICE_ADDRESS\n", progName);
}

int main(int argc, char **argv)
{
	if (argc < 4)
	{
		usage(argv[0]);
		exit(EXIT_FAILURE);
	}
	struct gatt_server_params params = {
		.key_file = argv[1],
		.cert_file = argv[2],
		.listening_port = PORT};
	deviceAddress = argv[3];
	SSL_load_error_strings();
	SSL_library_init();
	api_user = getenv("API_USER");
	api_password = getenv("API_PASSWORD");
	connection = gattlib_connect(NULL, deviceAddress, GATTLIB_CONNECTION_OPTIONS_LEGACY_DEFAULT);
	if (connection == NULL)
	{
		SSL_CTX_free(ssl_ctx);
		fprintf(stderr, "[ERROR] Fail to connect to the bluetooth device %s.\n", deviceAddress);
		return 1;
	}
	run(&params);
	return 0;
}