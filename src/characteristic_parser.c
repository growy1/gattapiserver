#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "gatt_api_server.h"



/** parse_and_json_format_temp_and_humidity from 'txx.xx,hxx.xx' format */
char *parse_and_json_format_temp_and_humidity(unsigned char *msg)
{
    char tempval[6] = {0};
    char humidval[6] = {0};
    bool validtemp = false;
    bool validhumid = false;
    char *formatted = malloc(1024);
    memset(formatted, 0, 1024);
    for (int i = 0; i < strlen((char *)msg);)
    {
        if (msg[i] == 't')
        {
            int j = 0;
            /** value is floating point with 2 decimal  from 00.00 -> 99.99 */
            i += 1;
            while (msg[i] != ',')
            {
#ifdef DEBUG
                printf("[DEBUG] Parsing temp : %c\n", msg[i]);
#endif /** DEBUG */
                tempval[j] = msg[i];
                j++;
                i++;
            }
            validtemp = true;
#ifdef DEBUG
            printf("[DEBUG] Got temp value : [%s]\n", tempval);
#endif /** DEBUG */
        }
        else if (msg[i] == 'h')
        {
            int j = 0;
            i += 1;
            while (msg[i] && msg[i] != ',')
            {
#ifdef DEBUG
                printf("[DEBUG] Parsing humid : %c\n", msg[i]);
#endif /** DEBUG */
                humidval[j] = msg[i];
                j++;
                i++;
            }
            validhumid = true;
#ifdef DEBUG
            printf("[DEBUG] Got humidity value : [%s]\n", humidval);
#endif /** DEBUG */
        }
        i++;
    }
    if (validtemp && validhumid)
    {
        char *time = getrfc3339utctimestamp();
        sprintf(formatted,
                "[{\"date\": \"%s\",\"type\": \"TEMPERATURE\",\"value\":%s,\"unit\": \"°c\"},{\"date\": \"%s\", \"type\": \"HUMIDITY\",\"value\":%s,\"unit\": \"RH\"}]",
                time, tempval, time, humidval);
        free(time);
    }
    return formatted;
}