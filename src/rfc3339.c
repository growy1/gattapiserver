#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 256
#define RFC_3339_NOFRAC_UTC "%Y-%m-%dT%H:%M:%SZ"

/** getrfc3339utctimestamp return RFC3339 timestamp string on utc format */
char *getrfc3339utctimestamp()
{
    char *result = malloc(MAX_SIZE);
    struct timespec tp;
    clockid_t clk_id;
    struct tm *time;
    size_t result_len;

    errno = 0;
    clk_id = CLOCK_REALTIME_COARSE;
    if (clock_gettime(clk_id, &tp) == -1)
    {
        fprintf(stderr, "[ERRO] Failed to getttime : %s\n", strerror(errno));
        return NULL;
    }
    time = gmtime(&tp.tv_sec);
    result_len = strftime(result, MAX_SIZE, RFC_3339_NOFRAC_UTC, time);
    if (result_len == 0)
    {
        free(result);
        return NULL;
    }
#ifdef DEBUG
    printf("[DEBUG] Got RFC3339 timestamp [%s]\n", result);
#endif /** DEBUG */
    return result;
}


/** gettimestamp return RFC3339 timestamp with time zone */
char *getrfc3339timestamp()
{
    char *buf = malloc(100);
    memset(buf, 0, 100);
    time_t now = time(NULL);
    struct tm *tm;
    int off_sign;
    int off;

    if ((tm = localtime(&now)) == NULL)
    {
        return NULL;
    }
    off_sign = '+';
    off = (int)tm->tm_gmtoff;
    if (tm->tm_gmtoff < 0)
    {
        off_sign = '-';
        off = -off;
    }
    sprintf(buf, "%d-%d-%dT%02d:%02d:%02d%c%02d:%02d",
            tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
            tm->tm_hour, tm->tm_min, tm->tm_sec,
            off_sign, off / 3600, off % 3600);
#ifdef DEBUG
    printf("[DEBUG] Got RFC3339 timestamp [%s]\n", buf);
#endif /** DEBUG */
    return buf;
}