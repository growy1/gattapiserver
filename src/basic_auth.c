#include <stdlib.h>
#include <string.h>
#include "global.h" /** api_user & api_password */
#include "gatt_api_server.h"

static char **getuserandpassword(char *decoded)
{
    int i = 0;
    int j = 0;
    char username[MAX_AUTH_USER_LEN] = {0};
    char password[MAX_AUTH_PASSWORD_LEN] = {0};
    if (strchr(decoded, ':') == NULL)
        return NULL;
    while (decoded[i] && decoded[i] != ':')
    {
        username[i] = decoded[i];
        if (i + 1 >= MAX_AUTH_USER_LEN)
            return NULL;
        i++;
    }
    if (i + 1 >= strlen(decoded))
    {
        return NULL;
    }
    i += 1;
    while (decoded[i])
    {
        password[j] = decoded[i];
        i++;
        if (j + 1 >= MAX_AUTH_PASSWORD_LEN)
            return NULL;
        j++;
    }
    char **user_and_password = malloc(sizeof(char **) * 2);
    user_and_password[0] = strdup(username);
    user_and_password[1] = strdup(password);
    return user_and_password;
}

bool check_auth(char *authorization)
{
    bool rt = false;
#ifdef DEBUG
    printf("[DEBUG] will decode auth [%s]\n", authorization);
#endif /* DEBUG */
    if (authorization == NULL)
        return rt;
    char *without_basic_string = strremove(authorization, "Basic ");
    char *decoded = base64decode(without_basic_string, strlen(without_basic_string));
#ifdef DEBUG
    printf("[INFO] Got decoded [%s]\n", decoded);
#endif /* DEBUG */
    if (decoded == NULL)
        return rt;
    char **user_and_pass = getuserandpassword(decoded);
    if (user_and_pass == NULL)
        return rt;
#ifdef DEBUG
    printf("[DEBUG] Got user [%s] and password [%s]\n", user_and_pass[0], user_and_pass[1]);
#endif /* DEBUG */
    /** If no api user / password specified, let them through */
    if (api_user == NULL || api_password == NULL)
    {
#ifdef DEBUG
        printf("[DEBUG] Not api user and password specified\n");
#endif /* DEBUG */
        return true;
    }
    if (strcmp(user_and_pass[0], api_user) == 0 &&
        strcmp(user_and_pass[1], api_password) == 0)
        rt = true;
    free(user_and_pass[0]);
    free(user_and_pass[1]);
    free(user_and_pass);
    return rt;
}