#include "gatt_api_server.h"
#include "global.h"

/** get_characteristics return channel uuids for a connected BLE device */
char **get_characteristics(int *count)
{
    gattlib_primary_service_t *services = NULL;
    gattlib_characteristic_t *characteristics = NULL;
    char uuid_str[MAX_LEN_UUID_STR + 1] = {0};
    int characteristics_count = 0;
    int valid_char_count = 0;
    int services_count = 0;
    int i = 0;

    int ret = gattlib_discover_primary(connection, &services, &services_count);
    if (ret != GATTLIB_SUCCESS)
    {
        fprintf(stderr, "[ERROR] Fail to discover primary services.\n");
        return NULL;
    }
    for (i = 0; i < services_count; i++)
    {
        gattlib_uuid_to_string(&services[i].uuid, uuid_str, sizeof(uuid_str));
#ifdef DEBUG
        printf("[DEBUG] discovered service[%d] uuid:%s\n", i, uuid_str);
#endif /* DEBUG */
    }
    free(services);
    ret = gattlib_discover_char(connection, &characteristics, &characteristics_count);
    if (ret != GATTLIB_SUCCESS)
    {
        fprintf(stderr, "[ERROR] Fail to discover characteristics.\n");
        return NULL;
    }
    char **res = malloc(sizeof(char **) * (characteristics_count));
    memset(res, 0, sizeof(char **) * characteristics_count);
    for (i = 0; i < characteristics_count; i++)
    {
        gattlib_uuid_to_string(&characteristics[i].uuid, uuid_str, sizeof(uuid_str));
#ifdef DEBUG
        printf("[DEBUG] characteristic n° %d value_handle:%04x uuid:%s\n", i, characteristics[i].value_handle, uuid_str);
#endif /* DEBUG */
        if (strlen(uuid_str) == UUID_LENGHT)
        {
            res[valid_char_count] = strdup(uuid_str);
            valid_char_count++;
        }
    }
    *count = valid_char_count;
    free(characteristics);
    return res;
}

/** read_message from connected ble device channel uuid */
unsigned char *read_message(uuid_t channelUuid)
{
    int ret;
    size_t len;
    uint8_t *buffer = NULL;

    ret = gattlib_read_char_by_uuid(connection, &channelUuid, (void **)&buffer, &len);

    if (ret != GATTLIB_SUCCESS)
    {
        char uuid_str[MAX_LEN_UUID_STR + 1];

        gattlib_uuid_to_string(&channelUuid, uuid_str, sizeof(uuid_str));

        if (ret == GATTLIB_NOT_FOUND)
        {
            fprintf(stderr, "[ERROR] Could not find GATT Channel (characteristic) with UUID %s.\n", uuid_str);
        }
        else if (ret == GATTLIB_ERROR_DBUS)
        {
            fprintf(stderr, "[ERROR] Device disconnect, will attempt to reconnect\n", uuid_str);
            gattlib_disconnect(connection);
            connection = gattlib_connect(NULL, deviceAddress, GATTLIB_CONNECTION_OPTIONS_LEGACY_DEFAULT);
            if (connection == NULL)
            {
                fprintf(stderr, "[ERROR] Fail to connect to the bluetooth device %s.\n", deviceAddress);
                return NULL;
            }
            else
            {
                return read_message(channelUuid);
            }
        }
        else
        {
            fprintf(stderr, "[ERROR] Error while reading GATT Characteristic with UUID %s (ret:%d)\n", uuid_str, ret);
        }
        return NULL;
    }
    if (len - 1 > 0 && buffer != NULL)
    {
        buffer[len - 1] = '\0';
    }
#ifdef DEBUG
    printf("[DEBUG] Read completed (%zu bytes received)\n", len);
    printf("[DEBUG] Got message [%s]\n", buffer);
#endif /* DEBUG */
    if (len < DATA_MIN_LEN)
    {
        fprintf(stderr, "[ERROR] Corrupted data received [%s]\n", buffer);
        free(buffer);
        return NULL;
    }
    return buffer;
}

/** write_message to ble connected device to rx channel UUID */
int write_message(uuid_t channelUuid, char *message)
{
    int ret;
    size_t len;
    char *endptr;
    long int value_data;
    uint8_t *buffer = NULL;
    char uuid_str[MAX_LEN_UUID_STR + 1];

    errno = 0;
    value_data = strtol(message, &endptr, 16);
    if (errno != 0)
    {
        fprintf(stderr, "[ERROR] could not convert message (strtol) : %s.\n", strerror(errno));
        return EXIT_FAILURE;
    }
    else if (endptr == message)
    {
        fprintf(stderr, "[ERROR] no digits were found in message [%s].\n", message);
        return EXIT_FAILURE;
    }
    ret = gattlib_write_without_response_char_by_uuid(connection, &channelUuid, &value_data, sizeof(value_data));
    gattlib_uuid_to_string(&channelUuid, uuid_str, sizeof(uuid_str));
    if (ret != GATTLIB_SUCCESS)
    {
        if (ret == GATTLIB_NOT_FOUND)
        {
            fprintf(stderr, "[ERROR] could not find GATT channel UUID %s.\n", uuid_str);
        }
        else
        {
            fprintf(stderr, "[ERROR] error while writing GATT channel UUID %s (ret:%d)\n",
                    uuid_str, ret);
        }
        return EXIT_FAILURE;
    }
#ifdef DEBUG
    printf("[DEBUG] Write message [%ld] (%s) to UUID %s\n", value_data, message, uuid_str);
#endif /* DEBUG */
    return EXIT_SUCCESS;
}