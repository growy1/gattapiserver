#include <unistd.h>
#include "gatt_api_server.h"

void add_stream(http2_session_data *session_data,
                http2_stream_data *stream_data)
{
    stream_data->next = session_data->root.next;
    session_data->root.next = stream_data;
    stream_data->prev = &session_data->root;
    if (stream_data->next)
    {
        stream_data->next->prev = stream_data;
    }
}

void remove_stream(http2_session_data *session_data,
                   http2_stream_data *stream_data)
{
    (void)session_data;

    stream_data->prev->next = stream_data->next;
    if (stream_data->next)
    {
        stream_data->next->prev = stream_data->prev;
    }
}

http2_stream_data *
create_http2_stream_data(http2_session_data *session_data, int32_t stream_id)
{
    http2_stream_data *stream_data;
    stream_data = malloc(sizeof(http2_stream_data));
    memset(stream_data, 0, sizeof(http2_stream_data));
    stream_data->stream_id = stream_id;
    stream_data->fd = -1;

    add_stream(session_data, stream_data);
    return stream_data;
}

void delete_http2_stream_data(http2_stream_data *stream_data)
{
    if (stream_data->fd != -1)
    {
        close(stream_data->fd);
    }
    free(stream_data->request_path);
    free(stream_data->method);
    if (stream_data->body)
    {
        free(stream_data->body);
    }
    if (stream_data->user_agent)
    {
        free(stream_data->user_agent);
    }
    if (stream_data->authorization)
    {
        free(stream_data->authorization);
    }
    free(stream_data);
}