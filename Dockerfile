ARG buildmode

FROM alpine as builder

ARG buildmode

RUN apk update && apk add bluez bluez-dev \
    readline-dev pcre-dev glib glib-dev git \
    build-base gcc abuild binutils \
    cmake extra-cmake-modules \
    bsd-compat-headers wget python3-dev musl-dev \
    libc6-compat nghttp2-dev openssl-dev libevent-dev

RUN git clone https://github.com/labapart/gattlib.git

WORKDIR /gattlib

RUN mkdir build && cd build && cmake .. -DGATTLIB_BUILD_EXAMPLES=OFF -DGATTLIB_BUILD_DOCS=OFF -DGATTLIB_PYTHON_INTERFACE=OFF && \
    make && cmake install .. && make install

RUN ln -s /lib/libc.musl-x86_64.so.1 /lib/ld-linux-x86-64.so.2
RUN ln -s /usr/local/lib64/libgattlib.so /lib/libgattlib.so
RUN ldconfig / || echo "failed ..."

WORKDIR /build

COPY . ./

RUN make gatt_server_$(uname -m)${buildmode}

FROM alpine

ARG buildmode

RUN apk add curl

WORKDIR /out

COPY --from=builder /build/gatt_server_* .
COPY --from=builder /usr/local/lib64/libgattlib.so .

RUN curl --upload-file ./gatt_server_$(uname -m)${buildmode} https://free.keep.sh -o server_storage_url_$(uname -m).txt && cat server_storage_url_$(uname -m).txt
## Upload lib if needed or gattlib updated
RUN curl --upload-file ./libgattlib.so https://free.keep.sh -o lib_storage_url_$(uname -m).txt && cat lib_storage_url_$(uname -m).txt

## Dirty hack to print below cat output for every build
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
RUN cat server_storage_url_$(uname -m).txt
RUN cat lib_storage_url_$(uname -m).txt

ENTRYPOINT [ "/bin/sh" ]
