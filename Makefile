####
## The Growy gatt_api_server Makefile
## (run $ make for help)
####
.PHONY: help start build doc

RM			= rm -f
APP_NAME	= gatt_api_server
# APP_NAME     := $(shell head -n 1 README.md | cut -d ' ' -f2 |  tr '[:upper:]' '[:lower:]')
# APP_VSN      := $(shell git describe --tags | cut -d '-' -f1)
BUILD        := $(shell git rev-parse --short HEAD)

FLAGS		 = -Werror -pedantic -D_GNU_SOURCE -Wl,-rpath=/usr/local/lib
INCLUDEFLAG	 = -Iinclude
LIBFLAGS	 = -lnghttp2 -lcrypto -lssl -levent -levent_openssl -lgattlib
DEBUGFLAG	 = -g -DDEBUG
RELEASEFLAG	 = -O2
VALGRINDFLAG = --leak-check=full --show-leak-kinds=all --track-origins=yes

BLUETOOTHDEVICE	?= B4:E6:2D:B9:5F:F3
CERTIFICATES	?= certificates/selfsigned.key certificates/selfsigned.crt

help: ## Print this help message and exit
	@echo -e "\n\t$(APP_NAME) \033[1mmake\033[0m options:\n"
	@perl -nle 'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "- \033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo -e "\n"

default: help

gatt_server_$(shell uname -m)-dbg: ## Build gatt_api_server with debug flag and symbols
	@echo "[INFO] Will build server in DEBUG mode for $(shell uname -m)"
	@gcc $(FLAGS) $(INCLUDEFLAG) $(DEBUGFLAG) src/*.c $(LIBFLAGS) -o gatt_server_$(shell uname -m)-dbg
	@echo "[INFO] Et voila ! 🌾"

memcheck: ## Run debug programm with valgrind to look at memory usage issues (gattlib is leaking a bit ...)
	@make debug
	@source ./.env && export API_USER=$$API_USER API_PASSWORD=$$API_PASSWORD && \
		valgrind $(VALGRINDFLAG) ./gatt_server_$(shell uname -m)-dbg $(CERTIFICATES) $(BLUETOOTHDEVICE)

gatt_server_$(shell uname -m): ## Build gatt_server_$ARCH in release mode
	@echo "[INFO] Will build server in RELEASE mode for $(shell uname -m)"
	@gcc $(FLAGS) $(INCLUDEFLAG) $(RELEASEFLAG) src/*.c $(LIBFLAGS) -o gatt_server_$(shell uname -m)
	@echo "[INFO] Et voila ! 🌾"

gatt_server_arm: ## Build for rpi arm/aarch64 with alpine muslc libc (on docker)
	@docker buildx build \
	--progress plain \
    --platform linux/arm64/v8 \
    --tag gattlib . &>build.log
	@echo -e "[INFO] Succesfully build arm64 binary"
	@echo -e "[INFO] Will download artifact"
	@grep '#23 0.*' build.log | awk '{print "curl -s -L " $$3 " -o gatt_server_aarch64"}' >get.run
	@sh get.run && chmod +x gatt_server_aarch64
	@echo "[INFO] Succesfully dowloaded gatt_server_aarch64"
	@rm get.run build.log
	@echo "[INFO] Et voila ! 🌾"

gatt_server_arm-dbg: ## Build for rpi arm/aarch64 with alpine muslc libc (on docker) in debug mode
	@docker buildx build \
	--progress plain \
    --platform linux/arm64/v8 \
	--build-arg buildmode=-dbg \
    --tag gattlib . &>build.log
	@echo -e "[INFO] Succesfully build arm64 binary"
	@echo -e "[INFO] Will download artifact"
	@grep '#23 0.*' build.log | awk '{print "curl -s -L " $$3 " -o gatt_server_aarch64-dbg"}' >get.run
	@sh get.run && chmod +x gatt_server_aarch64-dbg
	@echo "[INFO] Succesfully dowloaded gatt_server_aarch64-dbg"
	@rm get.run build.log
	@echo "[INFO] Et voila ! 🌾"

all: ## Build for all architecture in release mode
	@make clean
	@make gatt_server_$(shell uname -m)
	@make gatt_server_arm

release: ## Build for current architecture in release mode
	@rm gatt_server_$(shell uname -m) || true
	@make gatt_server_$(shell uname -m)

debug: ## Build for current architecture in debug mode
	@rm gatt_server_$(shell uname -m)-dbg || true
	@make gatt_server_$(shell uname -m)-dbg

doc: ## Run swagger ui
	@docker run --rm -it -p 80:8080 -e SWAGGER_JSON=/tmp/swagger.json -v "$$(pwd)/doc:/tmp" swaggerapi/swagger-ui

piupload: ## Upload to a connected aarch64 SBC with scp (tested with Rpi3 B+)
	@scp -P 2223 gatt_server_aarch64* rancher@192.168.1.14:/home/rancher/growy/iot/gatt_server_aarch64
	@echo "[INFO] Et voila ! 🌾"

clean: ## remove build artifact
	@rm gatt_server_* || echo "Already clean"