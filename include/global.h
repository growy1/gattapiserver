#include <openssl/ssl.h>
#include <gattlib.h>

extern unsigned char next_proto_list[256];
extern size_t next_proto_list_len;
extern SSL_CTX *ssl_ctx;
extern struct event_base *evbase;
extern char *api_user;
extern char *api_password;
extern char *deviceAddress;
extern gatt_connection_t *connection;