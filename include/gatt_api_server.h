#include <event.h>
#include <stdbool.h>
#include <event2/event.h>
#include <event2/listener.h>
#include <event2/bufferevent_ssl.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/conf.h>
#include <nghttp2/nghttp2.h>
#include <gattlib.h>

#define PORT "8080"

#define MAX_METHOD_VALUE_LEN 6
#define MAX_ERROR_RESPONSE_LEN 120

#define MAX_AUTH_USER_LEN 32
#define MAX_AUTH_PASSWORD_LEN 64

#define DATA_MIN_LEN 11
#define UUID_LENGHT 36

#define METHOD_GET "GET"
#define METHOD_PUT "PUT"
#define METHOD_POST "POST"

#define OUTPUT_WOULDBLOCK_THRESHOLD (1 << 16)

#define ARRLEN(x) (sizeof(x) / sizeof(x[0]))

#define MAKE_NV(NAME, VALUE)                                                    \
    {                                                                           \
        (uint8_t *)NAME, (uint8_t *)VALUE, sizeof(NAME) - 1, sizeof(VALUE) - 1, \
            NGHTTP2_NV_FLAG_NONE                                                \
    }

typedef struct gatt_server_params
{
    const char *key_file;
    const char *cert_file;
    const char *listening_port;
} gatt_server_params;


typedef struct app_context
{
    SSL_CTX *ssl_ctx;
    struct event_base *evbase;
} app_context;

typedef struct http2_stream_data
{
    struct http2_stream_data *prev, *next;
    char *body;
    char *method;
    char *user_agent;
    char *request_path;
    char *authorization;
    int32_t stream_id;
    int fd;
} http2_stream_data;

typedef struct http2_session_data
{
    struct http2_stream_data root;
    struct bufferevent *bev;
    app_context *app_ctx;
    nghttp2_session *session;
    char *client_addr;
} http2_session_data;

/**
 * main.c
 */
int on_request_recv(nghttp2_session *session,
                    http2_session_data *session_data,
                    http2_stream_data *stream_data);
int session_recv(http2_session_data *session_data);
int session_send(http2_session_data *session_data);
void initialize_nghttp2_session(http2_session_data *session_data);
http2_session_data *create_http2_session_data(app_context *app_ctx,
                                              int fd,
                                              struct sockaddr *addr,
                                              int addrlen);
void delete_http2_session_data(http2_session_data *session_data);
ssize_t send_callback(nghttp2_session *session, const uint8_t *data,
                      size_t length, int flags, void *user_data);
ssize_t file_read_callback(nghttp2_session *session, int32_t stream_id,
                           uint8_t *buf, size_t length,
                           uint32_t *data_flags,
                           nghttp2_data_source *source,
                           void *user_data);
int send_response(nghttp2_session *session, int32_t stream_id,
                  nghttp2_nv *nva, size_t nvlen, int fd);
int error_reply(nghttp2_session *session,
                http2_session_data *session_data,
                http2_stream_data *stream_data,
                const char *status,
                const char *message);
int send_server_connection_header(http2_session_data *session_data);
void initialize_app_context(app_context *app_ctx, SSL_CTX *ssl_ctx,
                            struct event_base *evbase);
/** 
 * utils.c
 */
uint8_t hex_to_uint(uint8_t c);
int check_path(const char *path);
char *strremove(char *str, const char *sub);
int ends_with(const char *s, const char *sub);
void string2hexString(char *input, char *output);
char *percent_decode(const uint8_t *value, size_t valuelen);

/**
 * ssl_utils.c 
 */
int next_proto_cb(SSL *ssl, const unsigned char **data,
                  unsigned int *len, void *arg);
int alpn_select_proto_cb(SSL *ssl, const unsigned char **out,
                         unsigned char *outlen, const unsigned char *in,
                         unsigned int inlen, void *arg);
SSL_CTX *create_ssl_ctx(const char *key_file, const char *cert_file);
SSL *create_ssl(SSL_CTX *ssl_ctx);
/**
 * http2_stream_data.c 
 */
void add_stream(http2_session_data *session_data,
                http2_stream_data *stream_data);
void remove_stream(http2_session_data *session_data,
                   http2_stream_data *stream_data);
http2_stream_data *create_http2_stream_data(http2_session_data *session_data, int32_t stream_id);
void delete_http2_stream_data(http2_stream_data *stream_data);
/**
 * http2_session_data.c 
 */
http2_session_data *create_http2_session_data(app_context *app_ctx,
                                              int fd,
                                              struct sockaddr *addr,
                                              int addrlen);
void delete_http2_session_data(http2_session_data *session_data);
int session_send(http2_session_data *session_data);
int session_recv(http2_session_data *session_data);
/**
 * nghttp_callbacks.c 
 */
int on_data_chunk_recv_callback(nghttp2_session *session, uint8_t flags,
                                int32_t stream_id, const uint8_t *data,
                                size_t len, void *user_data);
int on_frame_recv_callback(nghttp2_session *session,
                           const nghttp2_frame *frame, void *user_data);
int on_stream_close_callback(nghttp2_session *session, int32_t stream_id,
                             uint32_t error_code, void *user_data);
void readcb(struct bufferevent *bev, void *ptr);
void writecb(struct bufferevent *bev, void *ptr);
void eventcb(struct bufferevent *bev, short events, void *ptr);
void acceptcb(struct evconnlistener *listener, int fd,
              struct sockaddr *addr, int addrlen, void *arg);
int on_begin_headers_callback(nghttp2_session *session,
                              const nghttp2_frame *frame,
                              void *user_data);
int on_header_callback(nghttp2_session *session,
                       const nghttp2_frame *frame, const uint8_t *name,
                       size_t namelen, const uint8_t *value,
                       size_t valuelen, uint8_t flags, void *user_data);
int send_server_connection_header(http2_session_data *session_data);
ssize_t file_read_callback(nghttp2_session *session, int32_t stream_id,
                           uint8_t *buf, size_t length,
                           uint32_t *data_flags,
                           nghttp2_data_source *source,
                           void *user_data);
ssize_t send_callback(nghttp2_session *session, const uint8_t *data,
                      size_t length, int flags, void *user_data);
/**
 * gattlib_wrapper.c 
 */
unsigned char *read_message(uuid_t channelUuid);
int write_message(uuid_t channelUuid, char *message);
char **get_characteristics();

/**
 * characteristic_parser.c
 */
char *parse_and_json_format_temp_and_humidity(unsigned char *msg);

/**
 * rfc3339.c 
 */
char *getrfc3339utctimestamp();
char *getrfc3339timestamp();

/**
 * base64.c 
 */
char *base64encode(const void *b64_encode_this, int encode_this_many_bytes);
char *base64decode(const void *b64_decode_this, int decode_this_many_bytes);

/**
 * basic_auth.c 
 */
bool check_auth(char *authorization);