# GattAPIServer

GattAPIServer wrap [ngttp2](https://github.com/nghttp2/nghttp2) and [gattlib](https://github.com/labapart/gattlib) to read and write data to a **BLE** device through a convenient JSON API.

## Dependency

You will need to install event2, ssl and gattlib libraries